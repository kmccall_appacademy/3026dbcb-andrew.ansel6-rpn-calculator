class RPNCalculator
  def initialize
    @array = []
    @value = 0
  end

  def empty?
    puts @array
    if @array.length == 0 || @array == nil
       true
    end
    false
  end

  def value
    @value
  end

  def push(num)
    @array << num
  end

  def plus
    raise "calculator is empty" if @array.length == 0 || @array == nil
    @value = @array[-2] + @array[-1]
    @array.pop
    @array.pop
    @array << @value
  end

  def minus
    raise "calculator is empty" if @array.length == 0 || @array == nil
    @value = @array[-2] - @array[-1]
    @array.pop
    @array.pop
    @array << @value
  end

  def times
    raise "calculator is empty" if @array.length == 0 || @array == nil
    @value = @array[-2] * @array[-1]
    @array.pop
    @array.pop
    @array << @value
  end

  def divide
    raise "calculator is empty" if @array.length == 0 || @array == nil
    @value = @array[-2].to_f / @array[-1].to_f
    @array.pop
    @array.pop
    @array << @value
  end

  def tokens(string)
    @array = string.split(" ")
    ops = "+-*/"
    @array.each_with_index do |e,i|
      unless ops.include?(e)
        @array[i] = e.to_i
      else
        @array[i] = e.to_sym
      end
    end
    @array
  end

  def evaluate(string)
    tokens(string)
    until @array.length == 1
      @array.each_with_index do |e,i|
        if e.class == Symbol
          if e.to_s == "+"
            @value = @array[i-2] + @array[i-1]
            @array.delete_at(i-2)
            @array.delete_at(i-2)
            @array[i-2] = @value
          end

          if e.to_s == "-"
            @value = @array[i-2] - @array[i-1]
            @array.delete_at(i-2)
            @array.delete_at(i-2)
            @array[i-2] = @value
          end

          if e.to_s == "*"
            @value = @array[i-2] * @array[i-1]
            @array.delete_at(i-2)
            @array.delete_at(i-2)
            @array[i-2] = @value
          end

          if e.to_s == "/"
            @value = @array[i-2].to_f / @array[i-1].to_f
            @array.delete_at(i-2)
            @array.delete_at(i-2)
            @array[i-2] = @value
          end
        end
      end
    end
    @value
  end
end

#
# #######   Walkthrough Version - Much cleaner, much easier to understand, and much shorter. I need to work on my refactoring 
# class RPNCalculator
#   def initialize
#     @stack = []
#   end
#
#   def push(num)
#     @stack << num
#   end
#
#   def value
#     @stack.last
#   end
#
#   def plus
#     perform_operation(:+)
#   end
#
#   def minus
#     perform_operation(:-)
#   end
#
#   def times
#     perform_operation(:*)
#   end
#
#   def divide
#     perform_operation(:/)
#   end
#
#   def tokens(string)
#     tokens = string.split
#     tokens.map { |char| operation?(char) ? char.to_sym : Integer(char) }
#   end
#
#   def evaluate(string)
#     tokens = tokens(string)
#
#     tokens.each do |token|
#       case token
#       when Integer
#         push(token) #It knows to do it on itself
#       else
#         perform_operation(token)
#       end
#     end
#
#     value #Calling self.value
#   end
#
#   private
#
#   def operation?(char)
#       [:+, :-, :*, :/].include?(char.to_sym)
#   end
#
#   def perform_operation(op_symbol)
#     raise "calculator is empty" if @stack.size < 2
#
#     second_operand = @stack.pop
#     first_operand = @stack.pop
#
#     case op_symbol
#     when :+
#       @stack << first_operand + second_operand
#     when :-
#       @stack << first_operand - second_operand
#     when :*
#       @stack << first_operand * second_operand
#     when :/
#       @stack << first_operand.fdiv(second_operand)
#     else #Excellent case to have. Take note, we don't want to be changing anything so we need to put first and second back into the stack
#       @stack << first_operand
#       @stack << second_operand
#       raise "No such operation: #{op_symbol}"
#     end
#   end
#
# end
